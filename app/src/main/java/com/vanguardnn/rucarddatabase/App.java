package com.vanguardnn.rucarddatabase;

import android.app.Application;
import android.content.Context;

import com.vanguardnn.rucarddatabase.cache.CardsCache;

public class App extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
