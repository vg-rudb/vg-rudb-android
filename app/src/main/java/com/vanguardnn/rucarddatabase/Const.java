package com.vanguardnn.rucarddatabase;

public class Const {
    //Card fields
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String CLAN = "clan";
    public static final String TRIGGER = "trigger";
    public static final String GRADE = "grade";
    public static final String SKILL = "skill";
    public static final String POWER = "power";
    public static final String SHIELD = "shield";
    public static final String CRITICAL = "critical";
    public static final String NATION = "nation";
    public static final String RACE = "race";
    public static final String GIFT = "gift";
    public static final String ILLUSTRATOR = "illustrator";
    public static final String ABILITIES = "abilities";
    public static final String FLAVOR = "flavor";
    public static final String IMAGE = "image";

    //Ability fields
    public static final String KEYWORD = "keyword";
    public static final String TYPE = "type";
    public static final String CIRCLE = "circle";
    public static final String TEXT = "text";
    public static final String LIMITATION = "limitation";

    public class AbilityTypes {
        public static final String ACT = "ACT";
        public static final String AUTO = "AUTO";
        public static final String CONT = "CONT";
    }
}
