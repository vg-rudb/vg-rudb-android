package com.vanguardnn.rucarddatabase;

import android.widget.Toast;

import androidx.annotation.StringRes;

public class Func {
    public static void makeToast(String message) {
        Toast.makeText(App.getContext(), message, Toast.LENGTH_LONG).show();
    }

    public static String getString(@StringRes int stringId) {
        return App.getContext().getString(stringId);
    }
}
