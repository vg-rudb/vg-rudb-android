package com.vanguardnn.rucarddatabase.api_worker;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ApiService {

    public CardsService cardsApi;
    private static ApiService instance;

    public static ApiService getInstance() {
        if (instance == null) {
            instance = new ApiService();
        }

        return instance;
    }

    private ApiService() {
        cardsApi = new Retrofit.Builder().baseUrl("http://84.201.128.249:5566/api/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(CardsService.class);
    }


}
