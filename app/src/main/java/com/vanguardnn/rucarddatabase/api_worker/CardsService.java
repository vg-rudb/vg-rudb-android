package com.vanguardnn.rucarddatabase.api_worker;

import com.vanguardnn.rucarddatabase.cache.Card;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface CardsService {

    @GET("cards/getCards")
    Call<List<Card>> getAllCards();

    @GET("cards/getCardById")
    Call<Card> getCardById(@Query("sort") String cardId);

    @GET("cards/getCards")
    Call<Card> getCards(@QueryMap Map<String, String> filter);
}
