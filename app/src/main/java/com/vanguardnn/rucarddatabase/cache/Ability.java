package com.vanguardnn.rucarddatabase.cache;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vanguardnn.rucarddatabase.Const;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Ability {
    @JsonProperty(Const.KEYWORD) private String keyword;
    @JsonProperty(Const.TYPE) private String type;
    @JsonProperty(Const.CIRCLE) private String circle;
    @JsonProperty(Const.TEXT) private String text;
    @JsonProperty(Const.LIMITATION) private String limitation;

    @JsonProperty(Const.KEYWORD)
    public String getKeyword() {
        return keyword;
    }

    @JsonProperty(Const.KEYWORD)
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @JsonProperty(Const.TYPE)
    public String getType() {
        return type;
    }

    @JsonProperty(Const.TYPE)
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty(Const.CIRCLE)
    public String getCircle() {
        return circle;
    }

    @JsonProperty(Const.CIRCLE)
    public void setCircle(String circle) {
        this.circle = circle;
    }

    @JsonProperty(Const.TEXT)
    public String getText() {
        return text;
    }

    @JsonProperty(Const.TEXT)
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty(Const.LIMITATION)
    public String getLimitation() {
        return limitation;
    }

    @JsonProperty(Const.LIMITATION)
    public void setLimitation(String limitation) {
        this.limitation = limitation;
    }
}