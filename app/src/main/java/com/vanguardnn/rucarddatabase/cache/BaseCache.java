package com.vanguardnn.rucarddatabase.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public abstract class BaseCache<T extends CacheItem> extends Observable {

    protected Map<String, T> items = new HashMap<>();
    private List<Observer> observers = new ArrayList<>();

    @Override
    public synchronized void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public synchronized void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        notifyObservers(null);
    }

    @Override
    public void notifyObservers(Object arg) {
        for (Observer observer : observers) {
            observer.update(this, arg);
        }
    }

    public T getItemById(String itemId) {
        return items.get(itemId);
    }

    public List<T> getItems() {
        return new ArrayList<>(items.values());
    }

    protected void insertItem(T item) {
        if (item == null) {
            return;
        }

        items.put(item.getId(), item);
        notifyObservers(item);
    }

    protected void insertItems(List<T> items) {
        if (items == null) {
            return;
        }

        for (T item : items) {
            this.items.put(item.getId(), item);
        }
        notifyObservers();
    }

    protected abstract void initCache();
}
