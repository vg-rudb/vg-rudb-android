package com.vanguardnn.rucarddatabase.cache;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vanguardnn.rucarddatabase.Const;

public abstract class CacheItem {
    @JsonProperty(Const.ID) private String id;

    @JsonProperty(Const.ID)
    public String getId() {
        return id;
    }

    @JsonProperty(Const.ID)
    public void setId(String id) {
        this.id = id;
    }
}
