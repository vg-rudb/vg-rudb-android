package com.vanguardnn.rucarddatabase.cache;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vanguardnn.rucarddatabase.Const;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Card extends CacheItem {
    @JsonProperty(Const.NAME) private String name;
    @JsonProperty(Const.CLAN) private String clan;
    @JsonProperty(Const.TRIGGER) private String trigger;
    @JsonProperty(Const.GRADE) private int grade;
    @JsonProperty(Const.SKILL) private String skill;
    @JsonProperty(Const.POWER) private int power;
    @JsonProperty(Const.SHIELD) private int shield;
    @JsonProperty(Const.CRITICAL) private int critical;
    @JsonProperty(Const.NATION) private String nation;
    @JsonProperty(Const.RACE) private String race;
    @JsonProperty(Const.GIFT) private String gift;
    @JsonProperty(Const.ILLUSTRATOR) private String illustrator;
    @JsonProperty(Const.ABILITIES) private List<Ability> abilities = null;
    @JsonProperty(Const.FLAVOR) private String flavor;
    @JsonProperty(Const.IMAGE) private String image;

    @JsonProperty(Const.NAME)
    public String getName() {
        return name;
    }

    @JsonProperty(Const.NAME)
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(Const.CLAN)
    public String getClan() {
        return clan;
    }

    @JsonProperty(Const.CLAN)
    public void setClan(String clan) {
        this.clan = clan;
    }

    @JsonProperty(Const.TRIGGER)
    public String getTrigger() {
        return trigger;
    }

    @JsonProperty(Const.TRIGGER)
    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    @JsonProperty(Const.GRADE)
    public Integer getGrade() {
        return grade;
    }

    @JsonProperty(Const.GRADE)
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @JsonProperty(Const.SKILL)
    public String getSkill() {
        return skill;
    }

    @JsonProperty(Const.SKILL)
    public void setSkill(String skill) {
        this.skill = skill;
    }

    @JsonProperty(Const.POWER)
    public int getPower() {
        return power;
    }

    @JsonProperty(Const.POWER)
    public void setPower(int power) {
        this.power = power;
    }

    @JsonProperty(Const.SHIELD)
    public int getShield() {
        return shield;
    }

    @JsonProperty(Const.SHIELD)
    public void setShield(int shield) {
        this.shield = shield;
    }

    @JsonProperty(Const.CRITICAL)
    public int getCritical() {
        return critical;
    }

    @JsonProperty(Const.CRITICAL)
    public void setCritical(int critical) {
        this.critical = critical;
    }

    @JsonProperty(Const.NATION)
    public String getNation() {
        return nation;
    }

    @JsonProperty(Const.NATION)
    public void setNation(String nation) {
        this.nation = nation;
    }

    @JsonProperty(Const.RACE)
    public String getRace() {
        return race;
    }

    @JsonProperty(Const.RACE)
    public void setRace(String race) {
        this.race = race;
    }

    @JsonProperty(Const.GIFT)
    public String getGift() {
        return gift;
    }

    @JsonProperty(Const.GIFT)
    public void setGift(String gift) {
        this.gift = gift;
    }

    @JsonProperty(Const.ILLUSTRATOR)
    public String getIllustrator() {
        return illustrator;
    }

    @JsonProperty(Const.ILLUSTRATOR)
    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }

    @JsonProperty(Const.ABILITIES)
    public List<Ability> getAbilities() {
        return abilities;
    }

    @JsonProperty(Const.ABILITIES)
    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

    @JsonProperty(Const.FLAVOR)
    public String getFlavor() {
        return flavor;
    }

    @JsonProperty(Const.FLAVOR)
    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    @JsonProperty(Const.IMAGE)
    public String getImage() {
        return image;
    }

    @JsonProperty(Const.IMAGE)
    public void setImage(String image) {
        this.image = image;
    }
}