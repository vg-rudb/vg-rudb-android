package com.vanguardnn.rucarddatabase.cache;

import com.vanguardnn.rucarddatabase.Func;
import com.vanguardnn.rucarddatabase.R;
import com.vanguardnn.rucarddatabase.api_worker.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardsCache extends BaseCache<Card> {

    private static CardsCache instance;

    public static CardsCache getInstance() {
        if (instance == null) {
            instance = new CardsCache();
        }

        return instance;
    }

    private CardsCache() {
        initCache();
    }

    @Override
    protected void initCache() {
        ApiService.getInstance().cardsApi.getAllCards().enqueue(new Callback<List<Card>>() {
            @Override
            public void onResponse(Call<List<Card>> call, Response<List<Card>> response) {
                if (response.isSuccessful()) {
                    insertItems(response.body());
                } else {
                    Func.makeToast(Func.getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<List<Card>> call, Throwable t) {
                Func.makeToast(Func.getString(R.string.error));
            }
        });
    }
}
