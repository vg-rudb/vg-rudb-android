package com.vanguardnn.rucarddatabase.tasks;

public interface Callback<T> {
    void invoke(T param);
}
