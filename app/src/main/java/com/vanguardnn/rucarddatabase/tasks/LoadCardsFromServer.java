package com.vanguardnn.rucarddatabase.tasks;

import android.os.AsyncTask;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vanguardnn.rucarddatabase.App;
import com.vanguardnn.rucarddatabase.cache.Card;

import java.util.ArrayList;
import java.util.List;

public class LoadCardsFromServer extends AsyncTask<String, Void, List<Card>> {
    private Callback<Card> callback;

    public LoadCardsFromServer(Callback<Card> callback) {
        this.callback = callback;
    }

    @Override
    protected List<Card> doInBackground(String... cardIds) {
        final ArrayList<Card> cards = new ArrayList<>();
        RequestQueue requests = Volley.newRequestQueue(App.getContext());
        final ObjectMapper mapper = new ObjectMapper();

        for (String cardId: cardIds) {
            String url = String.format("https://vanguardcards.my1.ru/cards/%s.json", cardId);
            StringRequest request = new StringRequest(url, response -> {
                try {
                    Card card = mapper.readValue(response, Card.class);

                    if (callback != null) {
                        callback.invoke(card);
                    }
                } catch (Exception e) {
                    Toast.makeText(App.getContext(), "Parsing error", Toast.LENGTH_LONG).show();
                }
            }, error -> Toast.makeText(App.getContext(), "Request error " + error.toString(), Toast.LENGTH_LONG).show());

            requests.add(request);
        }

        requests.start();
        return cards;
    }
}
