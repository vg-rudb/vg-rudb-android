package com.vanguardnn.rucarddatabase.views;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vanguardnn.rucarddatabase.views.main.Fragments;
import com.vanguardnn.rucarddatabase.views.main.MainControl;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment<P> extends Fragment implements BaseView {

    protected Context context;
    protected P presenter;
    private MainControl mainControl;
    private Unbinder unbinder;
    private int layout;

    public BaseFragment(@LayoutRes int layout) {
        this.layout = layout;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layout, container, false);
        context = view.getContext();
        unbinder = ButterKnife.bind(this, view);

        init();
        return view;
    }

    @Override
    public void requestSwitchFragment(Fragments fragment, Bundle args) {
        if (mainControl != null) {
            mainControl.requestSwitchFragment(fragment, args);
        }
    }

    @Override
    public void requestSwitchBackFragment() {
        if (mainControl != null) {
            mainControl.requestSwitchFragmentBack();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    public void onBackPressed() {
        mainControl.requestSwitchFragmentBack();
    }

    public void setMainControl(MainControl mainControl) {
        this.mainControl = mainControl;
    }

    protected void init() {}
}
