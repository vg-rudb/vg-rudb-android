package com.vanguardnn.rucarddatabase.views;

import java.lang.ref.WeakReference;

public class BasePresenter<V> {

    private WeakReference<V> view;

    protected void bindView(V view) {
        this.view = new WeakReference<>(view);
    }

    protected boolean setupDone() {
        return view != null;
    }

    protected V view() {
        return view.get();
    }
}
