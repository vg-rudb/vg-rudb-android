package com.vanguardnn.rucarddatabase.views;

import android.os.Bundle;

import com.vanguardnn.rucarddatabase.views.main.Fragments;

public interface BaseView {
    void requestSwitchFragment(Fragments fragment, Bundle args);
    void requestSwitchBackFragment();
}
