package com.vanguardnn.rucarddatabase.views.cardInfo;

import com.vanguardnn.rucarddatabase.cache.Card;
import com.vanguardnn.rucarddatabase.cache.CardsCache;
import com.vanguardnn.rucarddatabase.views.BasePresenter;

public class CardInfoPresenter extends BasePresenter<CardInfoView> {

    private Card card;

    public CardInfoPresenter(CardInfoView view) {
        bindView(view);
    }

    public void applyArgs(String cardId) {
        card = CardsCache.getInstance().getItemById(cardId);
        updateView();
    }

    private void updateView() {
        if (setupDone()) {
            view().updateView(card);
        }
    }
}
