package com.vanguardnn.rucarddatabase.views.cardInfo;

import com.vanguardnn.rucarddatabase.cache.Card;
import com.vanguardnn.rucarddatabase.views.BaseView;

public interface CardInfoView extends BaseView {
    void updateView(Card card);
}
