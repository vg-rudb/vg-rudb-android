package com.vanguardnn.rucarddatabase.views.cardInfo;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vanguardnn.rucarddatabase.Const;
import com.vanguardnn.rucarddatabase.R;
import com.vanguardnn.rucarddatabase.cache.Ability;
import com.vanguardnn.rucarddatabase.cache.Card;
import com.vanguardnn.rucarddatabase.views.BaseFragment;

import butterknife.BindView;

public class FragmentCardInfo extends BaseFragment<CardInfoPresenter> implements CardInfoView {

    @BindView(R.id.card_info_image) ImageView cardImage;
    @BindView(R.id.card_info_rules) TextView cardRules;

    public FragmentCardInfo() {
        super(R.layout.fragment_card_info);
    }

    @Override
    protected void init() {
        super.init();

        presenter = new CardInfoPresenter(this);

        Bundle args = getArguments();

        if (args != null) {
            presenter.applyArgs(args.getString(Const.ID));
        }
    }

    @Override
    public void updateView(Card card) {
        StringBuilder abilityTextBuilder = new StringBuilder();

        for (Ability ability:card.getAbilities()) {
            abilityTextBuilder.append(ability.getText() + "\n");
        }

        Picasso.with(context).load(card.getImage()).into(cardImage);
        cardRules.setText(abilityTextBuilder);
    }
}
