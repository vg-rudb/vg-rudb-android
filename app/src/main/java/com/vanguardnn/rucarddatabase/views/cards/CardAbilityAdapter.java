package com.vanguardnn.rucarddatabase.views.cards;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vanguardnn.rucarddatabase.Const;
import com.vanguardnn.rucarddatabase.R;
import com.vanguardnn.rucarddatabase.cache.Ability;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardAbilityAdapter extends RecyclerView.Adapter<CardAbilityAdapter.CardAbilityViewHolder> {

    private List<Ability> abilities;

    public CardAbilityAdapter(List<Ability> abilities) {
        super();

        this.abilities = abilities;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CardAbilityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_ability,
                parent, false);
        return new CardAbilityAdapter.CardAbilityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardAbilityViewHolder holder, int position) {
        holder.bind(abilities.get(position));
    }

    @Override
    public int getItemCount() {
        return abilities.size();
    }

    class CardAbilityViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_card_ability_type) ImageView textAbilityType;
        @BindView(R.id.text_card_ability_circle) TextView textAbilityCircle;
        @BindView(R.id.text_card_ability_rules) TextView textAbilityRules;

        public void bind(Ability ability) {
            String abilityType = ability.getType();
            String abilityCircle = ability.getCircle();
            String abilityRules = ability.getText();

            textAbilityCircle.setText(abilityCircle);
            textAbilityRules.setText(abilityRules);

            switch (abilityType) {
                case Const.AbilityTypes.ACT: {
                    textAbilityType.setImageResource(R.drawable.act);
                    break;
                }
                case Const.AbilityTypes.AUTO: {
                    textAbilityType.setImageResource(R.drawable.auto);
                    break;
                }
                case Const.AbilityTypes.CONT: {
                    textAbilityType.setImageResource(R.drawable.cont);
                    break;
                }
                default: {
                    textAbilityType.setImageResource(R.color.transparent);
                    break;
                }
            }
        }

        public CardAbilityViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
