package com.vanguardnn.rucarddatabase.views.cards;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vanguardnn.rucarddatabase.R;
import com.vanguardnn.rucarddatabase.cache.Card;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.CardsViewHolder> {

    private CardsAdapter.OnItemClickListener listener;
    private List<Card> cardList = new ArrayList<>();

    @NotNull
    @Override
    public CardsViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card,
                parent,false);
        return new CardsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CardsViewHolder holder, int position) {
        holder.bind(cardList.get(position));
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    void setItems(List<Card> cards) {
        cardList.clear();
        cardList.addAll(cards);
        notifyDataSetChanged();
    }

    void setListener(CardsAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Card item);
    }

    class CardsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_item_image) ImageView cardImage;
        @BindView(R.id.card_item_name) TextView cardName;
        @BindView(R.id.card_item_power) TextView cardCP;
        @BindView(R.id.card_item_id) TextView cardId;
        @BindView(R.id.card_item_grade) TextView cardGrade;
        @BindView(R.id.card_item_clan) TextView cardClan;
        @BindView(R.id.card_item_abilities_recycler) RecyclerView recyclerCardAbilities;
        private CardAbilityAdapter abilityAdapter;

        void bind(Card card) {
            cardName.setText(card.getName());
            cardCP.setText(card.getPower() + "");
            cardId.setText(card.getId());
            cardGrade.setText(card.getGrade() + "");
            cardClan.setText(card.getClan());
            Picasso.with(itemView.getContext()).load(card.getImage()).into(cardImage);

            recyclerCardAbilities.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            abilityAdapter = new CardAbilityAdapter(card.getAbilities());
            recyclerCardAbilities.setAdapter(abilityAdapter);

            cardImage.setOnClickListener(view -> listener.onItemClick(card));
            itemView.setOnClickListener(view ->
                    recyclerCardAbilities.setVisibility(
                            recyclerCardAbilities.getVisibility() == View.GONE ? View.VISIBLE : View.GONE));
        }

        CardsViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
