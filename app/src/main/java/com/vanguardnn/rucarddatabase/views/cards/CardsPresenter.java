package com.vanguardnn.rucarddatabase.views.cards;

import android.os.Bundle;

import com.vanguardnn.rucarddatabase.Const;
import com.vanguardnn.rucarddatabase.cache.CacheItem;
import com.vanguardnn.rucarddatabase.cache.Card;
import com.vanguardnn.rucarddatabase.cache.CardsCache;
import com.vanguardnn.rucarddatabase.views.BasePresenter;
import com.vanguardnn.rucarddatabase.views.main.Fragments;

import java.util.Observable;
import java.util.Observer;

public class CardsPresenter extends BasePresenter<CardsView> implements Observer {

    public CardsPresenter(CardsView view) {
        bindView(view);
        CardsCache.getInstance().addObserver(this);
    }

    public void onItemClick(Card card) {
        Bundle args = new Bundle();

        args.putString(Const.ID, card.getId());

        if (setupDone()) {
            view().requestSwitchFragment(Fragments.cardInfo, args);
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (setupDone()) {
            view().updateAdapter(CardsCache.getInstance().getItems());
        }
    }
}
