package com.vanguardnn.rucarddatabase.views.cards;

import com.vanguardnn.rucarddatabase.cache.Card;
import com.vanguardnn.rucarddatabase.views.BaseView;

import java.util.List;

public interface CardsView extends BaseView {
    void updateAdapter(List<Card> cards);
}
