package com.vanguardnn.rucarddatabase.views.cards;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vanguardnn.rucarddatabase.R;
import com.vanguardnn.rucarddatabase.cache.Card;
import com.vanguardnn.rucarddatabase.views.BaseFragment;

import java.util.List;

import butterknife.BindView;

public class FragmentCards extends BaseFragment<CardsPresenter> implements CardsView {

    @BindView(R.id.recycler_cards_list_cards) RecyclerView recyclerCards;
    private CardsAdapter adapter;

    public FragmentCards() {
        super(R.layout.fragment_cards_list);
    }

    @Override
    protected void init() {
        super.init();

        presenter = new CardsPresenter(this);
        adapter = new CardsAdapter();
        adapter.setListener(item -> presenter.onItemClick(item));

        recyclerCards.setLayoutManager(new LinearLayoutManager(context));
        recyclerCards.setAdapter(adapter);
    }

    @Override
    public void updateAdapter(List<Card> cards) {
        adapter.setItems(cards);
    }
}
