package com.vanguardnn.rucarddatabase.views.main;

import com.vanguardnn.rucarddatabase.views.BaseFragment;
import com.vanguardnn.rucarddatabase.views.cardInfo.FragmentCardInfo;
import com.vanguardnn.rucarddatabase.views.cards.FragmentCards;

public class FragmentFactory {
    public static BaseFragment getFragmentById(Fragments fragment) {
        switch (fragment) {
            case cards: {
                return new FragmentCards();
            }
            case cardInfo: {
                return new FragmentCardInfo();
            }
            default: {
                return null;
            }
        }
    }
}
