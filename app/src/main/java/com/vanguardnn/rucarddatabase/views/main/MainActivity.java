package com.vanguardnn.rucarddatabase.views.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.vanguardnn.rucarddatabase.R;
import com.vanguardnn.rucarddatabase.views.BaseFragment;

import java.util.Stack;

public class MainActivity extends AppCompatActivity implements MainView {

    private MainPresenter presenter;
    private BaseFragment currentFragment;
    private Stack<BaseFragment> fragmentsHistory = new Stack<>();
    private boolean isBackDoublePressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        presenter = new MainPresenter(this);

        requestSwitchFragment(Fragments.cards, new Bundle());
    }

    @Override
    public void onBackPressed() {
        if (currentFragment != null) {
            currentFragment.onBackPressed();
        }

        if (isBackDoublePressed) {
            super.onBackPressed();
            return;
        }

        isBackDoublePressed = true;
        Toast.makeText(this, getString(R.string.exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> isBackDoublePressed = false, 2000);

    }

    @Override
    public void requestSwitchFragment(Fragments fragment, Bundle args) {
        BaseFragment fragmentToSwitch = FragmentFactory.getFragmentById(fragment);

        if (fragmentToSwitch != null) {
            fragmentToSwitch.setArguments(args);
            fragmentToSwitch.setMainControl(presenter);
            if (currentFragment != null) {
                fragmentsHistory.push(currentFragment);
            }
            currentFragment = fragmentToSwitch;

            switchFragment();
        }
    }

    @Override
    public void requestSwitchBackFragment() {
        if (fragmentsHistory.size() > 0) {
            currentFragment = fragmentsHistory.pop();

            switchFragment();
        }
    }

    private void switchFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragments_container, currentFragment)
                .commit();
    }
}

