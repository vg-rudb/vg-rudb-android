package com.vanguardnn.rucarddatabase.views.main;

import android.os.Bundle;

public interface MainControl {
    void requestSwitchFragment(Fragments fragment, Bundle args);
    void requestSwitchFragmentBack();
}
