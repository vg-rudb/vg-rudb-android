package com.vanguardnn.rucarddatabase.views.main;

import android.os.Bundle;

import com.vanguardnn.rucarddatabase.views.BasePresenter;

public class MainPresenter extends BasePresenter<MainView> implements MainControl {

    public MainPresenter(MainView view) {
        bindView(view);
    }

    @Override
    public void requestSwitchFragment(Fragments fragment, Bundle args) {
        if (setupDone()) {
            view().requestSwitchFragment(fragment, args);
        }
    }

    @Override
    public void requestSwitchFragmentBack() {
        if (setupDone()) {
            view().requestSwitchBackFragment();
        }
    }
}
